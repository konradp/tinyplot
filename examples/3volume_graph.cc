#include "../src/tinyplot.hpp"
#include "../src/tinydate.hpp"
#include <gtkmm/application.h>
#include <gtkmm/window.h>

int main(int argc, char** argv)
{
    using namespace Tinyplot;
    using namespace Tinydate;
    using namespace std;
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

    Gtk::Window w;
    w.set_title("Plot");

    Plot pl;
    pl.set_show_vol(true);

    // First dataset: a vector of entries
    vector<pair<date, float>> data1 {
	{ date("2015-01-01"), 0 },
	{ date("2015-01-02"), 5 },
	{ date("2015-01-03"), 10 },
	{ date("2015-01-04"), 15 },
	{ date("2015-01-05"), 20 },
	{ date("2015-01-06"), 25 },
	{ date("2015-01-07"), 30 },
	{ date("2015-01-08"), 35 },
	{ date("2015-01-09"), 40 },
	{ date("2015-01-10"), 45 }
    };
    
    // Second dataset. Note: We have a date missing in data set
    vector<pair<date, float>> data2 {
	{ date("2015-01-01"), 10 },
	{ date("2015-01-02"), 20 },
	{ date("2015-01-03"), 70 },
	{ date("2015-01-04"), 60 },
	{ date("2015-01-06"), 60 },
	{ date("2015-01-07"), 65 },
	{ date("2015-01-08"), 30 },
	{ date("2015-01-09"), 25 },
	{ date("2015-01-10"), 30 },
	{ date("2015-01-11"), 0 }
    };

    // Volume dataset
    vector<pair<date, float>> data_vol {
	{ date("2015-01-01"), 10 },
	{ date("2015-01-02"), 5 },
	{ date("2015-01-03"), 60 },
	{ date("2015-01-04"), 50 },
	{ date("2015-01-05"), 30 },
	{ date("2015-01-06"), 25 },
	{ date("2015-01-10"), 20 },
	{ date("2015-01-11"), 30 }
    };

    // add our datasets
    pl.add_data("set1", data1);
    pl.add_data("set2", data2);
    pl.add_data_vol(data_vol);
    // add vertical lines
    pl.add_hline("line2", 15);
    pl.add_hline("line3", 55);

    // Add and show our plot	
    w.add(pl);
    pl.show();

    return app->run(w);
}
