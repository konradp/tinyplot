#!/bin/bash

function usage {
    echo "Usage: compile.sh source.cc"
    exit
}
    
if [[ $1 == "" ]] || [[ $1 == ${1%.*} ]]; then
    usage
fi

g++ -o ${1%.*} $1 \
    -g \
    -Wno-psabi -Wall \
    --std=c++14 `pkg-config --cflags --libs gtkmm-3.0`
