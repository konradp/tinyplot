#include "../src/tinyplot.hpp"
#include <gtkmm/application.h>
#include <gtkmm/window.h>

int main(int argc, char** argv)
{
    using namespace Tinyplot;
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

    Gtk::Window w;
    w.set_title("Drawing Area");

    Plot pl;
    w.add(pl);
    pl.show();

    return app->run(w);
}
