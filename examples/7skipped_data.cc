#include "../src/tinyplot.hpp"
#include "../src/tinydate.hpp"
#include <gtkmm/application.h>
#include <gtkmm/window.h>

int main(int argc, char** argv)
{
    using namespace Tinyplot;
    using namespace Tinydate;
    using namespace std;
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

    Gtk::Window w;
    w.set_title("Plot");

    Plot pl;
    pl.set_show_vol(true);
    pl.set_mouse_on(true);
    pl.set_type("candlestick");

    // First dataset: a vector of entries
    vector< pair<Tinydate::date, vector<float>> > data {
	{ date("2015-01-01"), {10, 15, 8, 13} },
	{ date("2015-01-02"), {20, 30, 15, 15} },
	{ date("2015-01-03"), {15, 20, 10, 12} },
	{ date("2015-01-04"), {15, 10, 5,  10} },
	{ date("2015-01-05"), {15, 25, 12, 20} },
	{ date("2015-01-06"), {25, 28, 6, 10} },
	{ date("2015-01-07"), {30, 46, 24, 40} },
	// Skipped data here
	{ date("2015-01-09"), {40, 45, 30, 35} },
	{ date("2015-01-10"), {45, 50, 20, 44} }
    };

    // Volume dataset
    vector<pair<date, float>> data_vol {
	{ date("2015-01-01"), 10 },
	{ date("2015-01-02"), 5 },
	{ date("2015-01-03"), 60 },
	{ date("2015-01-04"), 50 },
	{ date("2015-01-05"), 30 },
	{ date("2015-01-06"), 25 },
	{ date("2015-01-07"), 25 },
	{ date("2015-01-08"), 14 },
	{ date("2015-01-09"), 10 },
	{ date("2015-01-10"), 20 },
	{ date("2015-01-11"), 30 }
    };

    // add our datasets
    pl.add_data("set1", data);
    pl.add_data_vol(data_vol);
    // add vertical lines
    pl.add_vline(date("2015-01-04"));

    // Add and show our plot	
    w.add(pl);
    pl.show();

    return app->run(w);
}
