#include "../src/tinyplot.hpp"
#include "../src/tinydate.hpp"
#include <gtkmm/application.h>
#include <gtkmm/box.h>
#include <gtkmm/notebook.h>
#include <gtkmm/window.h>

// Purpose: Fix bug where mouse coordinates display crashes when volume data not present

int main(int argc, char** argv)
{
    using namespace Tinyplot;
    using namespace Tinydate;
    using namespace std;
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

    Gtk::Window w;
    w.set_title("Plot");

    Plot pl;
    pl.set_show_vol(true);
    pl.set_type("candlestick");
    pl.set_mouse_on(true);

    // First dataset: a vector of entries
    vector< pair<Tinydate::date, vector<float>> > data {
	{ date("2015-01-01"), {10, 15, 8, 13} },
	{ date("2015-01-02"), {20, 30, 15, 15} },
	{ date("2015-01-03"), {15, 20, 10, 12} }
    };

    // data
    pl.add_data("set1", data); // data
    pl.add_vline(date("2015-01-04")); // vertical data

    // Add and show our plot
    Gtk::Box a(Gtk::ORIENTATION_VERTICAL, 10);
    w.add(a);
    Gtk::Notebook n;
    Gtk::Box b(Gtk::ORIENTATION_VERTICAL, 10);

    a.pack_start(n, Gtk::PACK_EXPAND_WIDGET);
    n.append_page(b, "Graph");
    b.pack_start(pl, Gtk::PACK_EXPAND_WIDGET);
    w.show_all_children();

    return app->run(w);
}
