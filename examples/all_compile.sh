#!/bin/bash
for f in *.cc; do
    echo "Compiling $f"
    g++ -o ${f%.*} $f `pkg-config --cflags --libs gtkmm-3.0` -Wno-psabi -Wall
done
