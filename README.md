# Tinyplot library
A small c++ plotting library 

## Usage
Place the src/tinyplot.hpp file somewhere in a c++ gtkmm project.
In the project, `include` the `.hpp` file.
The library's one and only class is accessible via `Tinyplot` namespace.
Then `.add` the Tinyplot object to a `Gtk::Box` in your window.

## Screenshots
![Tinyplot example](/img/tinyplot_line_chart.png?raw=true "Tinyplot line chart")
![Tinyplot example](/img/tinyplot_candlestick_chart.png?raw=true "Tinyplot candlestick chart")

## Dependencies
- gtkmm-3.0 (get from gtkmm.org/en/download.shtml)
- cairomm (get from cairographics.org/download)
- Tinydate library (get from gitlab.com/nickyfow/tinyplotlib/blob/master/src/tinydate.hpp)

