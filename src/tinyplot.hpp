#ifndef TINYPLOT_HPP
#define TINYPLOT_HPP
#include <cairomm/context.h>
#include <gtkmm/drawingarea.h>
#include <cmath>
#include <iostream>
#include <vector>
#include "tinydate.hpp"
// ver: 0.1
namespace Tinyplot {
using std::vector;
using std::pair;
using std::make_pair;
using std::string;
using std::to_string;
using std::map;
using std::cout;
using std::endl;

// TODO: Replace some pairs with struct (name, value) ?
// TODO: Make v_lines_ like h_lines: 'lines' collection should belong to the plot
// not to a specific dataset? Or should they? Because if they belong to dataset, you can just clear
// by removing the dataset

// Tinyplot takes data in the following format pair(date, float)
// 2015-02-01, 202.23

// Converted coordinates for mouse hovering over candlestick chart
struct mouse_xy {
    int y;
    int o;
    int h;
    int l;
    int c;
    int v;
};

// Helper
// data set is a vector of (date, int value)
class DataSets {

public:
    DataSets() {};
    virtual ~DataSets() {};

    auto // map<string, vector<pair<string,float>>>
    Points() { return point_sets_; }; // Points

    auto //map< string, vector<pair<Tinydate::date, vector<float>>> >;
    Ohlc() { return ohlc_set_; }; // Candlestick

    auto hlines() { return p_hlines; };
    int	size() { return point_sets_.size(); };

    // Add
    bool add(string name, vector<pair<Tinydate::date, float>> data) {
        point_sets_[name] = data;
        return true;
    };
    void add(string name, vector<pair<Tinydate::date, vector<float>>> data) {
        ohlc_set_[name] = data;
    };
    void add_hline(string name, float value) { p_hlines[name] = value; };

    // Clear
    void clear() { point_sets_.clear(); ohlc_set_.clear(); };

private:
    // Points
    map<string, vector<pair<Tinydate::date, float>>> point_sets_;
    // Candlestick. Note: This is a one-element map for now
    map<string, vector<pair<Tinydate::date, vector<float>>> > ohlc_set_;
    map<string, float>  p_hlines;
};

////////////////////////////////////////////////
// MAIN CLASS
////////////////////////////////////////////////
class Plot : public Gtk::DrawingArea
{
///////////////////////////////////////// PUBLIC
public:
Plot() {
    init();
    vol_h_ = 0;
    show_vol_ = false;
    plot_type_ = "point";
};

virtual ~Plot() {};

void init() {
    max_n_ = -10;
    margin_frac = 15;
    margin_ = 15;
    max_y_ = 0;
    min_y_ = 999999; // TODO: Bad, do not hardcode a 'large' value
    get_max();
    ResetRange();
};

// Access by calling plot.add_data(name, vector<pair<date,float>>)
int margin_frac;
Tinydate::date range_from;
Tinydate::date range_to;

// Settings: get/set
void set_show_vol(bool v) {
    show_vol_ = v;
    if(show_vol_) vol_h_ = 4*margin_;
    else vol_h_ = 0;
};

void set_mouse_on(bool v) { is_mouse_on_ = v; };
void set_type(string t) { plot_type_ = t; };
void set_zero_min(bool v) { zero_min_ = v; };

// Dataset methods
template<typename T>
void add_data(string name, T data) {
    // If no range yet defined, or if the new data widens the range
    bool rst_range = false;
    if( (range_from == Tinydate::date()) || (data[0].first < range_from) ) {
        range_from = data[0].first;
        rst_range = true;
    }
    if( (range_to == Tinydate::date()) || (range_to < data[data.size()-1].first) ) {
        range_to = data[data.size()-1].first;
        rst_range = true;
    }

    data_sets.add(name, data);
    if(rst_range && data_sets.Ohlc().size() > 0) ResetRange();
};

void clear_data() { data_sets.clear(); v_lines_.clear(); init(); };
void add_hline(string name, float value) { data_sets.add_hline(name, value); };
void add_vline(Tinydate::date d) { v_lines_.push_back(d); };

// volume dataset
void add_data_vol(vector<pair<Tinydate::date, float>> data) { data_vol_ = data; };

void get_max() {
    Tinydate::date cur_date;
    int n = 0;

    // POINTS/CANDLESTICK
    if(data_sets.Points().size() > 0) {
        // POINTS CHART
        for(auto& set : data_sets.Points()) {
            // Check each set: get max y
            auto max_it = max_element(begin(set.second), end(set.second),
                [](const pair<Tinydate::date, float>& left, const pair<Tinydate::date, float>& right) {
                    return left.second < right.second;
                });
	    
                if(!zero_min_) {
                    // Get min y
                    auto min_it = min_element(begin(set.second), end(set.second),
                    [](const pair<Tinydate::date, float>& left, const pair<Tinydate::date, float>& right) {
                        return left.second < right.second;
                    });
                    if( ((*min_it).second < min_y_) || (min_y_ == 0) ) min_y_ = (*min_it).second;
                }

                // Get number of days in range; include missing days in count
                Tinydate::date cur_date = range_from;
	    
                if((*max_it).second > max_y_) max_y_ = (*max_it).second;
            }
    }

    if(data_sets.Ohlc().size() > 0) {
        // CANDLESTICK CHART
        if(!data_sets.Ohlc().size()) return; // exit if empty
        vector<float> tmp_v;

        // TODO: If i=4 we can get memory leak. Why?
        for(int i = 1; i < 4; i++) {
            // Construct temp vector to get max
            // TODO: There should be a more efficient way of doing this
            tmp_v.clear();
            for(auto e : data_sets.Ohlc()) {
                // for each dataset
                for(auto l : e.second) {
                    // get all get all floats (note: one-element map)
                    tmp_v.push_back( (l.second)[i] );
                    if(!zero_min_ && (i == 2) && ((l.second)[i] < min_y_))
                        min_y_ = (l.second)[i]; // min y
                }
            }
            // Get max y for each price
            auto max_y = max_element(begin(tmp_v), end(tmp_v));
            if(*max_y > max_y_) max_y_ = *max_y;
        }
    }

// TODO: TODO: TODO: FIX INDENTS FROM HERE
	    // x max
	    cur_date = range_from;
	    while(cur_date <= range_to) {
	        n++;
	        cur_date.next_day();
	    }
	    if(n > max_n_) max_n_ = n;
	
	    // Volume
	    max_vol_ = 0;
	    if(show_vol_ && data_vol_.size()) {
	        auto max_vol__it = max_element(begin(data_vol_), end(data_vol_),
		    [](const pair<Tinydate::date, float>& left, const pair<Tinydate::date, float>& right) {
		        return left.second < right.second;
	        });
	        max_vol_ = (*max_vol__it).second;
	    }
    };

    void ResetRange() {
	if(data_sets.Ohlc().size() < 1) return;
	// Already checked size of Ohlc dataset
	// Map the x coords to indices of the data set
	int i = 0;
	vector<pair<Tinydate::date, int>> v;
	for(Tinydate::date dt = range_from; dt <= range_to; dt.next_day()) {
	    if(dt == data_sets.Ohlc().begin()->second[i].first) {
		v.push_back(make_pair(dt, i)); // found
		i++;
	    } else {
		v.push_back(make_pair(dt, -1)); // not found
	    }
	}
	range_ = v;
	return;
    };

    /**** DRAWING FUNCTIONS ****/
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
	// Exit if no data
	if( (plot_type_ == "point") && (data_sets.Points().size() <= 0) ) return false;
	else if( (plot_type_ == "candlestick") && (!data_sets.Ohlc().size()) ) return false;

	// Prepare canvas
    Gtk::Allocation allocation = get_allocation();
	win_w_ = allocation.get_width();
	win_h_ = allocation.get_height();
	margin_ = win_h_/margin_frac;
	cr->set_line_width(1);
	cr->translate(margin_, win_h_-margin_);
	
	get_max();
	PlotAxes(cr);
	DrawMousePosition(cr);

	// Set colour
	cr->set_source_rgb(0.0, 0.6, 0.6);
    
	// Plot graphs
	if(data_sets.Points().size() > 0) PlotPoints(cr);
	if(data_sets.Ohlc().size() > 0) PlotCandlestick(cr);
	if(show_vol_) PlotVolume(cr); // Volume
	PlotHorizLines(cr);
	PlotVertLines(cr);
	
	mouse_pointing_ = false;
        return true;
    };


    void PlotAxes(const Cairo::RefPtr<Cairo::Context>& cr) {
	// Axis
	cr->set_source_rgb(0.0, 0.0, 0.0);
	// x
	cr->move_to(0, 0);
	cr->line_to(win_w_, 0);
	cr->stroke();
	// y
	cr->move_to(0, 0);
	cr->line_to(0, -win_h_);
	cr->stroke();

	// Dividers
	// x
	for(int k = 0; k <= max_n_; k++) {
	    cr->move_to(transform_x(k), 0);
	    cr->line_to(transform_x(k), 0 + margin_/5);
	    cr->stroke();
	}
	// y
	for(int j = 0; j <= 10; j++) {
	    cr->move_to(0, transform_y(j*(max_y_/10)));
	    cr->line_to(0-margin_/5, transform_y(j*(max_y_/10)));
	    cr->stroke();
	}

	// Axis labels init
	Pango::FontDescription font;
	font.set_family("Monospace");
	font.set_weight(Pango::WEIGHT_NORMAL);
	auto x_from = create_pango_layout(range_from.GetString());
	auto y_from = create_pango_layout(to_string((int) min_y_));
	auto x_to = create_pango_layout(range_to.GetString());
	auto y_to = create_pango_layout(to_string((int) max_y_));
	x_from->set_font_description(font);
	y_from->set_font_description(font);
	x_to->set_font_description(font);
	y_to->set_font_description(font);

	// Axis labels: Calculate text width for offsets
	int lbl_h, x_lbl_width, y_lbl_width1, y_lbl_width2;
	x_from->get_pixel_size(x_lbl_width, lbl_h);
	y_from->get_pixel_size(y_lbl_width1, lbl_h);
	y_to->get_pixel_size(y_lbl_width2, lbl_h);

	// x label draw
	cr->move_to(0, lbl_h/2);
	x_from->show_in_cairo_context(cr);
	cr->move_to(transform_x(max_n_-1)-x_lbl_width, lbl_h/2);
	x_to->show_in_cairo_context(cr);
	
	// y label draw min
	cr->move_to(-lbl_h*2, transform(min_y_) + y_lbl_width1/2);
	cr->rotate(-M_PI/2);
	y_from->show_in_cairo_context(cr);
	cr->rotate(M_PI/2);
	
	// y label draw max
	cr->move_to(-lbl_h*2, transform_y(max_y_) - y_lbl_width2/2);
	cr->rotate(-M_PI/2);
	y_to->show_in_cairo_context(cr);
	cr->rotate(M_PI/2);
    };

    
    void PlotPoints(const Cairo::RefPtr<Cairo::Context>& cr) {
	for(auto& set : data_sets.Points()) {
	    int x = 0;
	    bool skipped = false;
	    Tinydate::date prev_date;
	    Tinydate::date cur_date = range_from;
    
	    pair<double, double> prev_point(0,0);
	    for(auto& i : set.second) {
		// Handle skipped data
		while(cur_date < i.first) {
		    cur_date.next_day();
		    x++;
		    skipped = true;
		}

		// Circle
		if(draw_points_) {
		    cr->arc((double) transform_x(x), (double) transform(i.second), 3.0, 0.0, 2.0 * M_PI);
		    cr->fill();
		}

		// line from prev to current
		if(!skipped) {
		    cr->move_to(prev_point.first, prev_point.second);
		    cr->line_to(transform_x(x), transform(i.second));
		    cr->stroke();
		}
		
		// Increment
		prev_point = make_pair(transform_x(x), transform(i.second));
		x++;
		cur_date.next_day();
		skipped = false;
	    } //for points
	} //for set
    };


    /* Candlestick chart */
    void PlotCandlestick(const Cairo::RefPtr<Cairo::Context>& cr) {
	if(!data_sets.Ohlc().size()) return; // exit if empty
	int x = 0;
	Tinydate::date prev_date;
	Tinydate::date cur_date = range_from;

	for(auto e : data_sets.Ohlc()) {
	    // for each dataset (one only anyway)
	    for(auto d : e.second) {
		// for each day
		// Handle skipped data
		while(cur_date < d.first) {
		    cur_date.next_day();
		    x++;
		}

		// lines colour
		cr->set_source_rgb(0.2, 0.2, 0.2);

		// 0=o, 1=h, 2=l, 3=c
		// Draw high/low lines
		// vert line
		cr->move_to(transform_x(x), transform(d.second[1]));
		cr->line_to(transform_x(x), transform(d.second[2]));
		cr->stroke();

		// Set colour: red or green
		if(d.second[3] >= d.second[0]) cr->set_source_rgb(0.0, 0.6, 0.0);
		else cr->set_source_rgb(0.6, 0.0, 0.0);

		// Draw box
		cr->move_to(transform_x(x)-2, transform(d.second[0]));
		cr->line_to(transform_x(x)-2, transform(d.second[3]));
		cr->line_to(transform_x(x)+2, transform(d.second[3]));
		cr->line_to(transform_x(x)+2, transform(d.second[0]));
		cr->line_to(transform_x(x)-2, transform(d.second[0]));
		cr->fill();
		cr->stroke();

		// Increment
		x++;
		cur_date.next_day();
	    }
	}
	
	// Reset colour
	cr->set_source_rgb(0.0, 0.6, 0.6);
    };


    void PlotVolume(const Cairo::RefPtr<Cairo::Context>& cr) {
	// Prepare canvas
	cr->set_line_width(1);

	// graph init
	int x = 0;
	Tinydate::date cur_date = range_from;
	cr->set_line_width(5);
	cr->set_source_rgb(0.0, 0.6, 0.6);

	// volume data
	for(auto& i : data_vol_) {
	    // skip empty data
	    while(cur_date < i.first) { x++; cur_date.next_day(); }

	    // Draw bar
	    cr->move_to(transform_x(x), transform_vol(0));
	    cr->line_to(transform_x(x), transform_vol(i.second));
	    cr->stroke();

	    // Increment
	    x++; cur_date.next_day();
	}
    };


    void DrawMousePosition(const Cairo::RefPtr<Cairo::Context>& cr) {
	    Pango::FontDescription font;
	    font.set_family("Monospace");
	    font.set_weight(Pango::WEIGHT_NORMAL);
    
	    // Mouse location display: O H L C v
	    string mouse_loc;
	    if(is_mouse_on_ && mouse_pointing_) {
	        mouse_loc =
	            string("y: ") + to_string(mouse_xy_.y) + string("\n")
	            + string("O: ") + to_string(mouse_xy_.o) + string("\n")
	            + string("H: ") + to_string(mouse_xy_.h) + string("\n")
	            + string("L: ") + to_string(mouse_xy_.l) + string("\n")
	            + string("C: ") + to_string(mouse_xy_.c) + string("\n")
	            + string("V: ") + to_string(mouse_xy_.v);
	    }
	    else mouse_loc = "y: \nO:\nH:\nL:\nC:\nV:";
	    auto mouse_loc_lbl = create_pango_layout(mouse_loc);
	    mouse_loc_lbl->set_font_description(font);
	    cr->move_to(transform_x(0), -(win_h_-margin_));
	    mouse_loc_lbl->show_in_cairo_context(cr);
    };


    void PlotHorizLines(const Cairo::RefPtr<Cairo::Context>& cr) {
	for(auto hline : data_sets.hlines()) {
	    cr->move_to(transform_x(0), transform(hline.second));
	    cr->line_to(transform_x(max_n_), transform(hline.second));
	    cr->stroke();
	}
    };

    
    void PlotVertLines(const Cairo::RefPtr<Cairo::Context>& cr) {
        cr->set_line_width(1);
	    Tinydate::date cur_date = range_from;
	    int x = 0;
	    size_t i = 0;

	    for(Tinydate::date cur_date = range_from; cur_date <= range_to; cur_date.next_day()) {
	        if(i < v_lines_.size()) {
		        if(v_lines_[i] == cur_date) {
		            // Found line. Draw it
		            vector<double> dashes = {40.0, 40.0};
		            cr->set_dash(dashes, 0.0);
		            cr->move_to(transform_x(x), 0);
		            cr->line_to(transform_x(x), -win_h_);
		            cr->stroke();
		            dashes.clear();
		            cr->set_dash(dashes, 0.0);
		            i++;
		        }
	        }
	        x++;
	    }
    };
    /**** END OF DRAWING FUNCTIONS ***/

    // VALUES -> SCREEN
    // Transform values into coordinates
    float transform_x(float v) {
	//int margin_ = win_w_/margin_frac;
	// 2*margin_ for left&right
	return margin_/2 + v*(win_w_-(4/2)*margin_)/(max_n_-1);
    };
    float transform(float v) {
	// Values above volume
	return - ( vol_h_ + (v-min_y_)*(win_h_-margin_*2-vol_h_)/(max_y_-min_y_));
    };
    float transform_y(float v) {
	//return (win_h_ - margin_ - vol_h_) - (v)*(win_h_-2*margin_-vol_h_)/(max_y_);
	//return (win_h_ - margin_ - extra_h_) - v*(win_h_-2*margin_-extra_h_)/max_y_;
	return - ( v*(win_h_-2*margin_-extra_h_)/max_y_ );
    };
    float transform_vol(float value) {
	// because of this, there is less margin_ on the bottom
	return - value*(vol_h_-margin_/8)/max_vol_;
    };


    // SCREEN -> VALUES
    // Transform coordinates from screen into dates or values
    float x_to_value(float v) {
        return (int) (
            (max_n_-1) *
            ( v - (((float) 3/2)*margin_) // left margin indent
                  + ( win_w_ - 2*margin_ )/(2*(max_n_-1)) )// shift2right, mid-bounds
        ) / ( win_w_ - 2*margin_ );
    };
    float y_to_value(float v) {
	    return (win_h_-margin_-vol_h_-v)*(max_y_-min_y_)
            /(win_h_-2*margin_-vol_h_) + min_y_;
    };


    // OVERRIDDEN METHODS
    virtual void on_show() {
	    // Initialise graph
	    if(is_mouse_on_) add_events(Gdk::SCROLL_MASK | Gdk::POINTER_MOTION_MASK);
	    Gtk::Widget::on_show();
    };


    // MOUSE EVENTS
    virtual bool on_motion_notify_event(GdkEventMotion* ev) {
	// TODO: If within range (e.g. date), return
	size_t x = (size_t) x_to_value(ev->x);
	int true_x = range_[x].second;

	if( (x + 1 > range_.size())
                || ((int) ev->x < (3/2)*margin_)
                || x < 0
                || (win_h_ - (int) ev->y < margin_) ) {
            // Outside range
            mouse_xy_ = { 0, 0, 0, 0, 0, 0 };
	    mouse_pointing_ = false;
	} else if(true_x == -1) {
            // No data
	    mouse_xy_ = { (int) y_to_value(ev->y), 0, 0, 0, 0, 0 };
	    mouse_pointing_ = false;
	} else {
            // Data present
	    auto data_v = data_sets.Ohlc().begin()->second[true_x].second;
	    mouse_xy_ = {
		(int) y_to_value(ev->y), // Y
		(int) data_v[0],  // O
		(int) data_v[1], // H
		(int) data_v[2], // L
		(int) data_v[3], // C
		CheckVol()? (int) data_vol_[x].second : 0 // V
	    };
            mouse_pointing_ = true;
	}
	
	queue_draw();
	return true;
    };
    
////////////////////////////////// PRIVATE
private:
    DataSets data_sets;
    vector<Tinydate::date> v_lines_;
    vector<pair<Tinydate::date, float>> data_vol_;

    // functions
    bool CheckVol() {
        auto a = data_sets.Points();
	if(a.size() > 0)
	    if((a.begin()->second).size() > 0)
                return true;
        return false;
    };

    // Main window
    int margin_;
    int win_w_, win_h_;

    // Graph
    string plot_type_;
    bool   draw_points_ = false;
    float  min_y_, max_y_;
    int    max_n_;
    bool   zero_min_ = false; // Scale graph by starting y axis from non-zero
    // This is for Ohlc and missing data
    vector<pair<Tinydate::date, int>> range_;

    // Volume graph
    bool  show_vol_;
    int   vol_h_; // Height
    int   extra_h_ = 0; // Extra space for multiple graphs
    float max_vol_;

    // Other
    bool  is_mouse_on_ = false;
    bool  mouse_pointing_ = false;
    mouse_xy mouse_xy_ = { 0, 0, 0, 0, 0};
    
}; // class Plot
}; // namespace Tinyplot
#endif // TINYPLOT_HPP
