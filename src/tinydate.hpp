#ifndef TINY_DATE_HPP
#define TINY_DATE_HPP
#include <ctime>
#include <iostream>
#include <vector>

namespace Tinydate {

class date {
// Takes string 2016-02-01 in the constructor
// Validates wrong dates, e.g. 31st Feb (Feb only has 30 days)
// Format: YYYY-MM-DD

public:
    // Constructor
    // Valid instantiations
    // date()
    // date("2018-04-01")
    // date(2018,4,1)
    date() {
        time_t t_now = time(0);
        struct tm *tm_now = localtime(&t_now);
        k_str = std::to_string(1900 + tm_now->tm_year) + "-"
              + pad_zeros(std::to_string(tm_now->tm_mon + 1)) + "-"
              + pad_zeros(std::to_string(tm_now->tm_mday));
    };
    date(std::string dt) {
        k_str = dt;
        init();
    };
    date(int y, int m, int d)
    : date(std::to_string(y) + std::string("-")
        + pad_zeros(std::to_string(m)) + std::string("-")
        + pad_zeros(std::to_string(d))) {};
    void init() { month_days[1] = is_leap()? 29 : 28; };

    ///// Set methods
    void set_year(std::string y) { k_str = y + "-" + month() + "-" + day(); };
    void set_month(std::string m) { k_str = year() + "-" + m + "-" + day(); };
    void set_day(std::string y) { k_str = year() + "-" + month() + "-" + y; };
    std::string set_date(std::string d) { k_str = d; return k_str; };

    // Incrementing (setting)
    std::string prev_month() { return set_date(get_prev_month().GetString()); };
    std::string next_month() { return set_date(get_next_month().GetString()); };
    std::string next_day() { return set_date(get_next_day().GetString()); };
    std::string prev_day() { return set_date(get_prev_day().GetString()); };

    ///// Get methods
    std::string GetString() const { return k_str; };
    std::string year_month() const { return year()+"-"+month(); };
    std::string year() const { return k_str.substr(0,4); };
    std::string month() const { return k_str.substr(5,2); };
    std::string day() const { return k_str.substr(8,2); };
    std::string end_of_month() {
        return year() + "-"
               + month() + "-"
               + std::to_string(month_days[stoi(month())-1]);
    };
    int day_of_week() {
        std::vector<int> v = { 0,3,2,5,0,3,5,1,4,6,2,4 };
        int y = stoi(year());
        int m = stoi(month());
        int d = stoi(day());
        y -= m < 3; // decrement if month less than 3
        return (y + y/4 - y/100 + y/400 +v[m-1] + d) % 7;
    };
    time_t GetTimestamp() {
        struct tm t = {
            0,                      // sec
            0,                      // min
            0,                      // hour
            std::stoi(day()),       // mday
            std::stoi(month())-1,   // month
            std::stoi(year())-1900, // year
        };
        return mktime(&t);
    };
    // Cheat method for seconds since epoch
    time_t GetRightNowTimestamp() {
        return time(0);
    };

    // Incrementing: return date
    // month
    date get_prev_month() {
        date a(GetString());
        if(stoi(a.month()) == 1) {
            a.set_month("12");
            a.set_year( std::to_string(stoi(a.year())-1) );
            init();
        } else { a.set_month( pad_zeros(std::to_string(stoi(a.month())-1)) ); }
        return a;
    };
    date get_next_month() {
        date a(GetString());
        if(stoi(a.month()) == 12) {
            a.set_month("01");
            a.set_year( std::to_string(stoi(a.year())+1) );
            init();
        } else {
            a.set_month( pad_zeros(std::to_string(stoi(a.month())+1)) );
        }
        return a;
    };
    date get_prev_month(int i) {
        date a(GetString());
        for(int j = 0; j < i; j++) a = a.get_prev_month();
        return a;
    };
    date get_next_month(int i) {
        date a(GetString());
        for(int j = 0; j < i; j++) a = a.get_next_month();
        return a;
    };

    // day
    date get_prev_day() {
        date a(GetString());
        if(a.day() == "01") {
            a.prev_month(); // also takes care of the year if necessary
            a.set_day( pad_zeros(std::to_string(a.month_days[stoi(a.month()) - 1])) );
        } else { a.set_day( pad_zeros(std::to_string(stoi(a.day()) - 1)) ); }
        return a;
    }
    date get_next_day() {
        date a(GetString());
        if(stoi(a.day()) == month_days[stoi(a.month())-1]) {
            a.next_month();
            a.set_day("01");
        } else { a.set_day( pad_zeros(std::to_string(stoi(a.day()) + 1)) ); }
        return a.GetString();
    };
    date get_prev_day(int i) {
        date a(GetString());
        for(int j=0; j<i; j++) a = a.get_prev_day();
        return a;
    };
    date get_next_day(int i) {
        date a(GetString());
        for(int j=0; j<i; j++) a = a.get_next_day();
        return a;
    };

    ///// Check methods
    bool is_valid() { return is_valid(k_str); };;
    bool is_leap() { return is_leap(stoi(year())); };
    bool is_set() { return (GetString() == "1900-01-01")? false : true; };

    /* overload operators */
    // TODO: Can this be simplified?
    bool operator< (const date &m_date) {
        if(stoi(year()) == stoi(m_date.year())) {
            if(stoi(month()) == stoi(m_date.month())) {
                if(stoi(day()) == stoi(m_date.day())) return false;
                else return (stoi(day()) < stoi(m_date.day()));
            } else return (stoi(month()) < stoi(m_date.month()));
        } else return (stoi(year()) < stoi(m_date.year()));
    };
    bool operator== (const date &m_date) {
        return (GetString() == m_date.GetString())? true : false;
    };
    bool operator<= (const date &m_date) {
        if((*this < m_date) || (*this == m_date)) return true;
        else return false;
    };

private:
    bool is_leap(int yr) {
        bool leap = false;
        if(yr % 4 == 0) {
            leap = true;
            if(yr % 100 == 0) {
                leap = false;
                if(yr % 400 == 0) leap = true;
            }
        }
        return leap;
    };

    bool is_valid(std::string s) {
        // Check digits are digits
        for(int i: {0,1,2,3,5,6,8,9}) {if(!isdigit(s[i])) return false;}
        // Check hyphens are hyphens
        for(int i: {4,7}) {if(s[i] != '-') return false;}
        // No of days in a month
        if(stoi(day()) > month_days[stoi(month())-1]) return false;
        // No of months <= 12
        if(stoi(month()) > 12) return false;
        // all fine
        return true;
    };

    std::string k_str;
    int month_days[12] = {
        31, 28, 31, 30,
        31, 30, 31, 31,
        30, 31, 30, 31};
    std::string pad_zeros(std::string a) { return (stoi(a) < 10)? "0" + a : a; };


}; /* end class date */
}; /* end namespace tinydate */
#endif // TINY_DATE_HPP
